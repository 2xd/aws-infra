# Dos pasos por delante

https://dospasospordelante.org/ Es un proyecto con base abierta que busca ayudar en la prevención de soluciones a los proyectos que están por venir (o podrían estar por venir) en la situación actual respecto al COVID-19.
Nace desde local (Córdoba) pero desde el minuto 0 es exportable y conectable a lo global, por eso lanzamos en abierto.

# aws-infra

Este repositorio contiene la parte de DynamoDB y Infraestructura en AWS.

Una base de datos DynamoDB que permite tener dos tablas:
- Gente que ofrece su ayuda
- Personas que lo necesitan

# Ayuda

Si quieres ayudar, hay grupo en Telegram, entra sólo si puedes y quieres ayudar con AWS / DynamoDB, por favor:
https://t.me/joinchat/AEvnSBjYWP_btCj63E3U2g